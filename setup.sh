#!/bin/bash
#SBATCH -J aettari
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --time=100:00:00
#SBATCH --partition=gpu
#SBATCH --mem=32GB
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=8


module load Miniconda3/4.9.2
conda create --name aettari_env python=3.10
source activate aettari_env

pip install -r requirements.txt

# srun python train_UNet_PCA.py
srun python train_UNet.py