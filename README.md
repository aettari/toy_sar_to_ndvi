# toy_SAR_TO_NDVI


This is just a toy example to see if my model works well. The aim of this toy example is to see if I can increase the resolution of the images using a diffusion model. I'll be using as input a dataset taken from kaggle (check the Data section) and downsampled using gaussian blur. The target will be the actual data.

## 1. Data

I'm going to use a dataset taken from kaggle, about actor pictures (https://www.kaggle.com/datasets/vishesh1412/celebrity-face-image-dataset?select=Celebrity+Faces+Dataset). There are 1800 pictures of 12 actors. The pictures are not shaped the same, therefore, I'm going to shape them in the same way.

