# from models import UNet, EMA
from TOY_EXAMPLE.models.TOY_model_31.simple_UNet_model_12_toy import SimpleUnet
import torch
from TOY_EXAMPLE.utils_toy import get_data,forward_alpha_compositing, custom_weighted_sampling, custom_weighted_sampling_2, unsharp_masker, forward_alpha_compositing_NEW
import torch.optim as optim
import logging
from tqdm import tqdm
import torch.nn as nn
from torch.utils.data import  DataLoader
import os
from torch.utils.tensorboard import SummaryWriter
from TOY_EXAMPLE.models.TOY_model_31.PERCEPTUAL_LOSS import VGGPerceptualLoss
# from pytorch_msssim import MS_SSIM 

def train(args):
    '''
    This function is used to train the model. It takes as input the arguments from the parser:
    - device: cpu or cuda
    - time_steps: number of time (alpha) steps
    - epochs: number of epochs
    - batch_size: batch size
    - root_dir: path to the data
    - continue_training: if True, it will continue the training from the weight_path
    - weight_path: path to the weight to load if continue_training is True
    - lr: learning rate
    - starting_epoch: if continue_training is True, it will start from this epoch
    - type_alpha_schedule: type of alpha schedule (linear_1, linear_2, cosine or sigmoid)
    - model_name: name of the model
    - TOY_EXAMPLE_folder: path to the TOY_EXAMPLE folder
    - custom_t (bool): if True, it will use the custom_weighted_sampling() function to take the t values
    - radius, percent, max_unsharps: parameters for the unsharp masking

    It returns nothing, but it saves the weights of the model in the folder weights.
    '''
    device = args.device
    time_steps = args.time_steps
    epochs = args.epochs
    batch_size = args.batch_size
    root_dir = args.root_dir
    continue_training = args.continue_training
    weight_folder = args.weight_folder
    weight_path = args.weight_path # the weight path is the path of the specific weight and not of the folder
    lr = args.lr
    starting_epoch = args.starting_epoch
    type_alpha_schedule = args.type_alpha_schedule
    model_name = args.model_name
    TOY_EXAMPLE_folder = args.TOY_EXAMPLE_folder
    custom_t = args.custom_t
    radius, percent, max_unsharps = args.radius, args.percent, args.max_unsharps

    # Get the Dataset and convert it into a dataloader
    train_dataset = get_data(root_dir, split='train')
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    val_dataset = get_data(root_dir, split='val')
    val_loader = DataLoader(val_dataset, batch_size=batch_size)
    
    # Parameters for the early stopping
    patience = 30  # Number of epochs with no improvement after which training will be stopped
    min_delta = 0.0001  # Minimum change in the monitored quantity to qualify as an improvement
    best_loss = float('inf')  # Initialize the best loss to a large value
    counter = 0  # Counter to keep track of epochs with no improvement

    # Create the model
    model = SimpleUnet().to(device)
    os.makedirs(weight_folder, exist_ok=True)
    # If the model is a continuation of a previous training, load the weights
    if continue_training:
      ckpt = torch.load(weight_path, map_location=torch.device(device))
      model.load_state_dict(ckpt)
      train_writer = SummaryWriter(log_dir = f'{TOY_EXAMPLE_folder}/models/{model_name}/logs_{model_name}/train_{starting_epoch}_StartEpoch_{batch_size}_BatchSize_{lr}_LR_{time_steps}_TimeSteps')
      val_writer = SummaryWriter(log_dir=f'{TOY_EXAMPLE_folder}/models/{model_name}/logs_{model_name}/val_{starting_epoch}_StartEpoch_{batch_size}_BatchSize_{lr}_LR_{time_steps}_TimeSteps')
    else:
      train_writer = SummaryWriter(log_dir = f'{TOY_EXAMPLE_folder}/models/{model_name}/logs_{model_name}/train_0_StartEpoch_{batch_size}_BatchSize_{lr}_LR_{time_steps}_TimeSteps')
      val_writer = SummaryWriter(log_dir=f'{TOY_EXAMPLE_folder}/models/{model_name}/logs_{model_name}/val_0_StartEpoch_{batch_size}_BatchSize_{lr}_LR_{time_steps}_TimeSteps')

    model.train()
    # Define the optimizer and the loss function
    # optimizer = optim.AdamW(model.parameters(), lr=lr)
    # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    # ms_ssim_module = MS_SSIM(data_range=1, size_average=True, channel=1)
    perc_loss = VGGPerceptualLoss()
    mae = nn.L1Loss()

    # Start the training
    for epoch in range(epochs):
        running_train_loss = 0.0
        running_val_loss = 0.0
        logging.info(f"Starting epoch {epoch}:")
        pbar_train = tqdm(train_loader)
        pbar_val = tqdm(val_loader)

        for i, (images_input,images_target) in enumerate(pbar_train): # i is the current batch iteration
            images_input = images_input.to(device)
            images_target = images_target.to(device)

            images_difference = images_target-images_input
            # unsharp_target = unsharp_masker(images_target, radius=radius, percent=percent, max_unsharps=max_unsharps).to(device)
            batch_size = images_input.shape[0] # we need to set batch_size otherwise, when the left images are less than the batch_size, we will have an error (when computing the loss)
        
            # In all_alpha_imgs there will be tensors shaped (batch_size, time_steps, 224, 224), of this tensor we want just to take a 
            # tensor shaped (batch_size, 1, 224, 224) (check the explanation below). To do that, we'll take a random t for each batch.
            # You can choose between the custom t and the random uniform t. If you choose the custom t, then you'll give more
            # probability of being taken to the last time steps (last starting from 0 and not T) and less probability to the first time steps.

            # 50% OF THE TIMES TAKE AS INPUT THE BLENDED IMAGE AND THE OTHER 50% OF THE TIMES TAKE THE COMPLETELY BLURRED IMAGE
            if torch.rand(1).item()>0.5:
              if custom_t:
                t = custom_weighted_sampling_2(time_steps, batch_size).to(device)
              else:
                t = torch.randint(low=1, high=time_steps, size=(batch_size,)).to(device)
            else:
              t = torch.randint(low=time_steps-1, high=time_steps, size=(batch_size,)).to(device)

            # Get the alpha images and the alpha target (Notice that is not like the DDPM, because we cannot move directly to the alpha image at the t step.
            # Therefore we need to perform all the forward steps (thanks to the forward_alpha_compositing() function) and then take the alpha
            # image at the t step for each image in the batch)). In all_alpha_imgs there will be tensors shaped
            # (batch_size, time_steps, 224, 224) (the width and the height are the ones from the SAR-NDVI problem).
            # Summarizing, we want just to take a "noised" image for each batch image. We take it randomly (according to t). 
            # So, if there are 50 images for each batch (representing the 50 time steps), we take just one of these 50 for 
            # each image in the batch and these will be saved in alpha_img.
            
            # all_alpha_imgs, all_alpha_target = forward_alpha_compositing(images_input, unsharp_target, t_steps=time_steps, device=device, type_schedule=type_alpha_schedule)
            all_alpha_imgs, all_alpha_target = forward_alpha_compositing_NEW(images_input, images_difference, t_steps=time_steps, device=device, type_schedule=type_alpha_schedule)
            alpha_img = [all_alpha_imgs[i][t[i]] for i in range(len(all_alpha_imgs))] # shaped (batch_size, 224, 224) because our images are 224x224 (but notice that it's a list)
            # The indexing [t[i]] is due to the fact that we want to take the alpha image at the t step corresponding to the image in the batch (t is a tensor of integers long
            # as the batch_size; each element in the batch must have the t in the corresponding position).


            # all_alpha_target is a tensor indicating the schedule of the alpha target values for each target image.
            # We need to assign to each image the alpha target value depending on its time step, t. (ACTUALLY, I DON'T
            # USE THIS INFORMATION, SO I COULD ALSO DELETE IT).
            all_alpha_target = [all_alpha_target[i] for i in t]
            all_alpha_target = torch.stack(all_alpha_target).unsqueeze(1).to(device) # torch tensor shaped (batch_size, 1)

            alpha_img = torch.stack(alpha_img).to(device) # torch tensor shaped (batch_size, 224, 224)
            alpha_img = alpha_img.unsqueeze(1) # torch tensor shaped (batch_size, 1, 224, 224)
            
            predicted_images_difference = model(alpha_img, t).reshape(images_target.shape)
            predicted_target_img = images_input+predicted_images_difference
            # train_loss = 1-ms_ssim_module(predicted_target_img, images_target)
            train_loss = 1*perc_loss(predicted_target_img, images_target)+0.5*mae(predicted_target_img, images_target)

            optimizer.zero_grad() # Clear gradients
            train_loss.backward() # Compute gradients
            optimizer.step() # Update weights
            pbar_train.set_postfix(PERC_LOSS_L1=train_loss.item()) # Update progress bar

            running_train_loss += train_loss.item()
        
        train_writer.add_images('alpha_images', alpha_img, epoch)
        train_writer.add_images('predicted_images', predicted_target_img, epoch)
        train_writer.add_images('target_images', images_target, epoch)

        running_train_loss /= len(train_loader)
        train_writer.add_scalar('Loss/train (PERC_LOSS + L1)', running_train_loss, epoch)
        

        with torch.no_grad():
          for i, (images_input,images_target) in enumerate(pbar_val):
            images_input = images_input.to(device)
            images_target = images_target.to(device)
            images_difference = images_target-images_input
            batch_size = images_input.shape[0] # we need to set batch_size otherwise, when the left images are 
            # less than the batch_size, we will have an error (when computing the loss)

            # Take a random time step for each image in the batch
            t = torch.randint(low=1, high=time_steps, size=(batch_size,)).to(device)

            all_alpha_imgs, all_alpha_target = forward_alpha_compositing_NEW(images_input, images_difference, t_steps=time_steps, device=device, type_schedule=type_alpha_schedule)
            alpha_img = [all_alpha_imgs[i][t[i]] for i in range(len(all_alpha_imgs))] # shaped (batch_size, 224, 224) because our images are 224x224 (but notice that it's a list)
            all_alpha_target = [all_alpha_target[i] for i in t]
            all_alpha_target = torch.stack(all_alpha_target).unsqueeze(1).to(device) # torch tensor shaped (batch_size, 1)

            alpha_img = torch.stack(alpha_img).to(device) # torch tensor shaped (batch_size, 224, 224)
            alpha_img = alpha_img.unsqueeze(1) # torch tensor shaped (batch_size, 1, 224, 224)
  
            predicted_val_images_difference = model(alpha_img, t).reshape(images_target.shape)
            predicted_val_target_image = images_input+predicted_val_images_difference
            # val_loss = 1-ms_ssim_module(predicted_val_target_image, images_target)
            val_loss = 1*perc_loss(predicted_val_target_image, images_target)+0.5*mae(predicted_val_target_image, images_target)

            pbar_val.set_postfix(PERC_LOSS_L1=val_loss.item()) # Update progress bar

            running_val_loss += val_loss.item()

        running_val_loss /= len(val_loader)
        val_writer.add_scalar('Loss/val (PERC_LOSS + L1)', running_val_loss, epoch)

        if running_val_loss < best_loss - min_delta:
          best_loss = running_val_loss
          counter = 0 
        else:
          counter += 1

        if counter >= patience:
           print('Early stopping! Training stopped')
           break
          
        # if the epoch is a multiple of 10, save the weights of the model
        if epoch % 10 == 0:
          if continue_training:
            torch.save(model.state_dict(), os.path.join(weight_folder, f"ckpt_{epoch+starting_epoch+1}_epoch_{args.batch_size}_batchsize.pt"))
          else:
            torch.save(model.state_dict(), os.path.join(weight_folder, f"ckpt_{epoch}_epoch_{args.batch_size}_batchsize.pt"))
          for weight in os.listdir(weight_folder):
            name_list = weight.split('_')
            if (int(name_list[1])<epoch-patience) & (int(name_list[1])<epoch+starting_epoch+1-patience):
              os.remove(os.path.join(weight_folder, weight))
            # if (weight != f"ckpt_{epoch+starting_epoch}_epoch_{args.batch_size}_batchsize.pt") & (weight != f"ckpt_{epoch}_epoch_{args.batch_size}_batchsize.pt"):
            #   os.remove(os.path.join(weight_folder, weight))
    
    
    train_writer.close()
    val_writer.close()

    if len(os.listdir(weight_folder))>1:
      min_epoch=10000
      for weight in os.listdir(weight_folder):
        name_list = weight.split('_')
        if int(name_list[1])<min_epoch:
          min_epoch = int(name_list[1])

      for weight in os.listdir(weight_folder):
        name_list = weight.split('_')
        if int(name_list[1])!=min_epoch:
          os.remove(os.path.join(weight_folder, weight))

def launch():
    import argparse
    parser = argparse.ArgumentParser()
    args, unknown = parser.parse_known_args()
    args.time_steps = 100
    args.epochs = 501
    args.batch_size = 16
    args.TOY_EXAMPLE_folder = os.getcwd()+os.path.sep+'TOY_EXAMPLE'
    args.root_dir = f'{args.TOY_EXAMPLE_folder}/DATA_bilinear'
    args.device = 'cuda'
    args.model_name = 'TOY_model_31'
    args.weight_folder = f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights'
    args.continue_training = False
    args.starting_epoch = 160
    args.weight_path = f'{args.weight_folder}/ckpt_{args.starting_epoch}_epoch_{args.batch_size}_batchsize.pt'
    args.lr = 3e-4
    args.type_alpha_schedule = 'sigmoid'
    args.custom_t = False
    args.radius, args.percent, args.max_unsharps = 3, 70, 7

    train(args)

if __name__ == '__main__':
    launch()
