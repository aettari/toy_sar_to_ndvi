import torch.nn as nn
import math
import torch
import torch.nn.functional as F

class Block(nn.Module):
    def __init__(self, in_ch, out_ch, up=False):
        super().__init__()
        if up:
            self.conv1 = nn.Conv2d(2*in_ch, out_ch, 3, padding=1)
            self.transform = nn.ConvTranspose2d(out_ch, out_ch, 4, 2, 1)
        else:
            self.conv1 = nn.Conv2d(in_ch, out_ch, 3, padding=1)
            self.transform = nn.Conv2d(out_ch, out_ch, 4, 2, 1)
        self.conv2 = nn.Conv2d(out_ch, out_ch, 3, padding=1)
        self.relu = nn.ReLU()
        self.batch_norm1 = nn.BatchNorm2d(out_ch)
        self.batch_norm2 = nn.BatchNorm2d(out_ch)
        
    def forward(self, x):
        # First Conv
        h = self.relu(self.batch_norm1(self.conv1(x)))
        # Second Conv
        h = self.relu(self.batch_norm2(self.conv2(h)))
        # Down or Upsample
        return self.transform(h)


class SimpleUnet(nn.Module):
    """
    A simplified variant of the Unet architecture.
    """
    def __init__(self):
        super().__init__()
        image_channels = 1 # it takes just one channel as input because we don't pass the stacked SAR, but the alpha_blending image generated from it
        down_channels = (64, 128, 256, 512)
        up_channels = (512, 256, 128, 64)
        out_dim = 1 # it takes just one channel as output because the ndvi has just one channel


        # Initial projection
        self.conv0 = nn.Conv2d(image_channels, down_channels[0], 3, padding=1)

        # Downsample
        self.downs = nn.ModuleList([Block(down_channels[i], down_channels[i+1]) \
                    for i in range(len(down_channels)-1)])
        # Upsample
        self.ups = nn.ModuleList([Block(up_channels[i], up_channels[i+1], up=True) \
                    for i in range(len(up_channels)-1)])
        # Output
        self.output = nn.Conv2d(up_channels[-1], out_dim, 1)


    def forward(self, x):

        # Initial conv
        x = self.conv0(x)
        
        # Unet
        residual_inputs = []
        for down in self.downs:
            x = down(x)
            residual_inputs.append(x)
        
        for up in self.ups:
            residual_x = residual_inputs.pop() # I need to start from the last one and moving to the first one; that's why we use .pop()
            # Add residual x as additional channels
            if residual_x.shape != x.shape:
                x = F.interpolate(x, size=residual_x.shape[-2:], mode='bilinear', align_corners=False)
            x = torch.cat((x, residual_x), dim=1) # residual connection between layers (1024 with 1024; 512 with 512 etc.)           
            x = up(x)
        return self.output(x)

model = SimpleUnet()
print("Num params: ", sum(p.numel() for p in model.parameters()))
model