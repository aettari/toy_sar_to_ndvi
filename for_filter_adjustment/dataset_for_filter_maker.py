import torch
from TOY_EXAMPLE.models.TOY_model_31.simple_UNet_model_12_toy import SimpleUnet
from TOY_EXAMPLE.utils_toy import alpha_schedule
import numpy as np
import os
import shutil



def dataset_for_filter_maker(args):
    '''
    This function saves the backward predictions of the trained diffusion model for 100 images. Each image step of the backward is saved,
    therefore, since there are 100 time steps, in the final dataset there will be 100*100=10000 images. In the name of the image, the first
    number is the time step of the image (e.g. t0_Brad_Pitt_35.pt).
    '''
    input_img_path_list = args.input_img_path_list
    target_img_path_list = args.target_img_path_list
    device = args.device
    weight_path = args.weight_path
    type = args.type
    time_steps_grid = args.time_steps_grid
    dataset_path = args.dataset_path

    os.makedirs(dataset_path, exist_ok=True)
    os.makedirs(os.path.join(dataset_path,'input_imgs'), exist_ok=True)
    os.makedirs(os.path.join(dataset_path,'target_imgs'), exist_ok=True)

    ckpt = torch.load(weight_path, map_location=torch.device(device))
    model = SimpleUnet().to(device)
    # model = UNet().to(device)
    model.load_state_dict(ckpt)

    all_alpha_input, all_alpha_target = alpha_schedule(t_steps=time_steps_grid, type=type)

    def min_max_scale(image):
        scaled_images = []
        for channel in range(image.shape[0]):
            channel_image = image[channel]
            min_val = channel_image.min()
            max_val = channel_image.max()
            scaled_channel_image = (channel_image - min_val) / (max_val - min_val)
            scaled_images.append(scaled_channel_image)

        scaled_tensor = torch.stack(scaled_images)
        return scaled_tensor


    with torch.no_grad():
        for input_img_path,target_img_path in zip(input_img_path_list, target_img_path_list):
            input_img = torch.load(input_img_path).to(device)
            target_img = torch.load(target_img_path).to(device)
            new_img_path = input_img_path.split('/')[-1]             
            for i in reversed(range(time_steps_grid)):
                if i == time_steps_grid-1:
                    alpha_blend_img = input_img*all_alpha_input[i] + target_img*all_alpha_target[i] # At the first step we don't have the prediction (the alpha blending is just composed by the input img)
                else:
                    alpha_blend_img = input_img*all_alpha_input[i]+ predicted_target_img[0]*all_alpha_target[i]
                alpha_blend_img = alpha_blend_img.to(torch.float32).unsqueeze(0).to(device)
                predicted_target_img = model(alpha_blend_img, torch.tensor([i], dtype=torch.float32).to(device))
                predicted_difference_target_img = model(alpha_blend_img, torch.tensor([i], dtype=torch.float32).to(device))
                predicted_target_img = input_img+predicted_difference_target_img
                predicted_target_img = min_max_scale(predicted_target_img)

                torch.save(predicted_target_img, os.path.join(dataset_path,'input_imgs',f't{i}_{new_img_path}'))

def input_target_splitter(args):
    '''
    This function iterates over the input images and since the target will be the same images but of the next time step (t2 of a certain image
    will be the target of t1 of the same image) this function will take into account just the images from t1 to t99. These images will be saved
    into the target_imgs folder but renamed the new name will be the same of the old one but with the t1 replaced by t0, the t2 replaced
    by t1 and so on. This because in order to be correctly read by the DataLoader, the input and the target images must have the same name.
    '''
    dataset_path = args.dataset_path
    
    for img_path in os.listdir(os.path.join(dataset_path, 'input_imgs')):
        elements = img_path.split('_')
        if int(elements[0][1:]) in range(1,100):
            img = torch.load(os.path.join(dataset_path, 'input_imgs', img_path))
            new_img_path = 't'+str(int(elements[0][1:])-1)+'_'+'_'.join(elements[1:])
            torch.save(img[0], os.path.join(dataset_path, 'target_imgs', new_img_path)) # the [0] is due to the fact that we want (1,1,height,width) tensors, but now we have (1,1,1,height,width)

    for img_path in os.listdir(os.path.join(dataset_path, 'input_imgs')):
        if img_path not in os.listdir(os.path.join(dataset_path, 'target_imgs')):
            os.remove(os.path.join(dataset_path, 'input_imgs', img_path))

def train_val_test_splitter(args):
    '''
    This function creates the folder of the dataset and splits the dataset into train and test sets.
    The train and test sets are created by randomly selecting the images from the input_path and 
    groundtruth_path folders. Eventually, this function removes the input_path and the groundtruth_path folders
    
    INPUT:
        * datafolder: path to the folder where the train and test folders will be created
        * input_path: path to the folder containing the input images
        * groundtruth_path: path to the folder containing the groundtruth images
        * train_size: percentage of the dataset to be used for training
        * val_size: percentage of the dataset to be used for validation
    
    OUTPUT:
        * None
    '''
    dataset_path = args.dataset_path
    input_path = os.path.join(dataset_path, 'input_imgs')
    groundtruth_path = os.path.join(dataset_path, 'target_imgs')
    train_size = args.train_size
    val_size = args.val_size

    import random
    os.makedirs(os.path.join(dataset_path, 'train', 'target_imgs'), exist_ok=True)
    os.makedirs(os.path.join(dataset_path, 'train', 'input_imgs'), exist_ok=True)
    os.makedirs(os.path.join(dataset_path, 'val', 'target_imgs'), exist_ok=True)
    os.makedirs(os.path.join(dataset_path, 'val', 'input_imgs'), exist_ok=True)
    os.makedirs(os.path.join(dataset_path, 'test', 'target_imgs'), exist_ok=True)
    os.makedirs(os.path.join(dataset_path, 'test', 'input_imgs'), exist_ok=True)

    shuffled_idxs = list(range(len(os.listdir(input_path))))
    random.shuffle(shuffled_idxs)
    train_idxs = shuffled_idxs[:int(len(shuffled_idxs)*train_size)]
    val_idxs = shuffled_idxs[int(len(shuffled_idxs)*train_size):int(len(shuffled_idxs)*(train_size+val_size))]
    test_idxs = shuffled_idxs[int(len(shuffled_idxs)*(train_size+val_size)):]
    
    file_names = os.listdir(input_path)
    
    for idx in train_idxs:
        shutil.copy(os.path.join(input_path, file_names[idx]), os.path.join(dataset_path, 'train', 'input_imgs', file_names[idx]))
        shutil.copy(os.path.join(groundtruth_path, file_names[idx]), os.path.join(dataset_path, 'train', 'target_imgs', file_names[idx]))
    for idx in val_idxs:
        shutil.copy(os.path.join(input_path, file_names[idx]), os.path.join(dataset_path, 'val', 'input_imgs', file_names[idx]))
        shutil.copy(os.path.join(groundtruth_path, file_names[idx]), os.path.join(dataset_path, 'val', 'target_imgs', file_names[idx]))
    for idx in test_idxs:
        shutil.copy(os.path.join(input_path, file_names[idx]), os.path.join(dataset_path, 'test', 'input_imgs', file_names[idx]))
        shutil.copy(os.path.join(groundtruth_path, file_names[idx]), os.path.join(dataset_path, 'test', 'target_imgs', file_names[idx]))

    shutil.rmtree(input_path)
    shutil.rmtree(groundtruth_path)


def launch():
    import argparse
    import os
    parser = argparse.ArgumentParser()
    args, unknown = parser.parse_known_args()
    args.TOY_EXAMPLE_folder = os.getcwd()+"/TOY_EXAMPLE"

    random_100_integers = np.random.choice(range(len(os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/'))),100,replace=False)
    args.input_img_path_list = [os.path.join(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/',os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/')[i]) for i in random_100_integers]
    args.device = 'cuda'
    args.model_name = 'TOY_model_31'
    args.weight_path = f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights/'+os.listdir(f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights/')[0]
    args.time_steps_grid = 100
    args.target_img_path_list = [os.path.join(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/target_imgs/',os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/target_imgs/')[i]) for i in random_100_integers]
    args.type = 'sigmoid'
    args.dataset_path = f'{args.TOY_EXAMPLE_folder}/DATA_FOR_FILTER'
    args.train_size = 0.7
    args.val_size = 0.2

    dataset_for_filter_maker(args)
    input_target_splitter(args)
    train_val_test_splitter(args)

if __name__ == '__main__':
    launch()