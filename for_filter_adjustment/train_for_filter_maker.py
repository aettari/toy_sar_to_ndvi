
from simple_UNet_model_for_filter_maker import SimpleUnet
import torch
from TOY_EXAMPLE.utils_toy import get_data
import torch.optim as optim
import logging
from tqdm import tqdm
import torch.nn as nn
from torch.utils.data import  DataLoader
import os
from torch.utils.tensorboard import SummaryWriter
from TOY_EXAMPLE.models.TOY_model_31.PERCEPTUAL_LOSS import VGGPerceptualLoss


def train(args):
    '''
    This function is used to train the model. It takes as input the arguments from the parser:
    - device: cpu or cuda
    - epochs: number of epochs
    - batch_size: batch size
    - root_dir: path to the data
    - continue_training: if True, it will continue the training from the weight_path
    - weight_folder: path to the folder where the weights will be saved
    - weight_path: path to the weight to load if continue_training is True
    - lr: learning rate
    - starting_epoch: if continue_training is True, it will start from this epoch

    It returns nothing, but it saves the weights of the model in the folder weights.
    '''
    device = args.device
    epochs = args.epochs
    batch_size = args.batch_size
    root_dir = args.root_dir
    continue_training = args.continue_training
    weight_folder = args.weight_folder
    weight_path = args.weight_path # the weight path is the path of the specific weight and not of the folder
    lr = args.lr
    starting_epoch = args.starting_epoch

    os.makedirs(weight_folder, exist_ok=True)


    # Get the Dataset and convert it into a dataloader
    train_dataset = get_data(root_dir, split='train')
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    val_dataset = get_data(root_dir, split='val')
    val_loader = DataLoader(val_dataset, batch_size=batch_size)
    
    # Parameters for the early stopping
    patience = 30  # Number of epochs with no improvement after which training will be stopped
    min_delta = 0.0001  # Minimum change in the monitored quantity to qualify as an improvement
    best_loss = float('inf')  # Initialize the best loss to a large value
    counter = 0  # Counter to keep track of epochs with no improvement

    # Create the model
    model = SimpleUnet().to(device)
    os.makedirs(weight_folder, exist_ok=True)
    # If the model is a continuation of a previous training, load the weights
    if continue_training:
      ckpt = torch.load(weight_path, map_location=torch.device(device))
      model.load_state_dict(ckpt)
      train_writer = SummaryWriter(log_dir = f'{os.getcwd()}/logs_filter_maker/train_{starting_epoch}_StartEpoch_{batch_size}_BatchSize_{lr}_LR')
      val_writer = SummaryWriter(log_dir=f'{os.getcwd()}/logs_filter_maker/val_{starting_epoch}_StartEpoch_{batch_size}_BatchSize_{lr}_LR')
    else:
      train_writer = SummaryWriter(log_dir = f'{os.getcwd()}/logs_filter_maker/train_0_StartEpoch_{batch_size}_BatchSize_{lr}_LR')
      val_writer = SummaryWriter(log_dir=f'{os.getcwd()}/logs_filter_maker/val_0_StartEpoch_{batch_size}_BatchSize_{lr}_LR')

    model.train()
    # Define the optimizer and the loss function
    # optimizer = optim.AdamW(model.parameters(), lr=lr)
    # optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    # ms_ssim_module = MS_SSIM(data_range=1, size_average=True, channel=1)
    perc_loss = VGGPerceptualLoss()
    # mae = nn.L1Loss()
    mse = nn.MSELoss()

    # Start the training
    for epoch in range(epochs):
        running_train_loss = 0.0
        running_val_loss = 0.0
        logging.info(f"Starting epoch {epoch}:")
        pbar_train = tqdm(train_loader)
        pbar_val = tqdm(val_loader)

        for i, (images_input,images_target) in enumerate(pbar_train): # i is the current batch iteration
            images_input = images_input.to(device)
            images_target = images_target.to(device)

            # images_difference = images_target-images_input
  
            batch_size = images_input.shape[0] # we need to set batch_size otherwise, when the left images are less than the batch_size, we will have an error (when computing the loss)
            
            # predicted_images_difference = model(alpha_img, t).reshape(images_target.shape)
            # predicted_target_img = images_input+predicted_images_difference
            predicted_target_img = model(images_input).reshape(images_target.shape)

            # train_loss = 1-ms_ssim_module(predicted_target_img, images_target)
            train_loss = 1*perc_loss(predicted_target_img, images_target)+0.5*mse(predicted_target_img, images_target)

            optimizer.zero_grad() # Clear gradients
            train_loss.backward() # Compute gradients
            optimizer.step() # Update weights
            pbar_train.set_postfix(PERC_LOSS_L2=train_loss.item()) # Update progress bar

            running_train_loss += train_loss.item()
        
        train_writer.add_images('input_images', images_input, epoch)
        train_writer.add_images('predicted_images', predicted_target_img, epoch)
        train_writer.add_images('target_images', images_target, epoch)

        running_train_loss /= len(train_loader)
        train_writer.add_scalar('Loss/train (PERC_LOSS + L2)', running_train_loss, epoch)
        

        with torch.no_grad():
          for i, (images_input,images_target) in enumerate(pbar_val):
            images_input = images_input.to(device)
            images_target = images_target.to(device)
            # images_difference = images_target-images_input
            batch_size = images_input.shape[0] # we need to set batch_size otherwise, when the left images are 
            # less than the batch_size, we will have an error (when computing the loss)
  
            # predicted_val_images_difference = model(alpha_img, t).reshape(images_target.shape)
            # predicted_val_target_image = images_input+predicted_val_images_difference
            predicted_val_target_image = model(images_input).reshape(images_target.shape)
            # val_loss = 1-ms_ssim_module(predicted_val_target_image, images_target)
            val_loss = 1*perc_loss(predicted_val_target_image, images_target)+0.5*mse(predicted_val_target_image, images_target)

            pbar_val.set_postfix(PERC_LOSS_L2=val_loss.item()) # Update progress bar

            running_val_loss += val_loss.item()

        running_val_loss /= len(val_loader)
        val_writer.add_scalar('Loss/val (PERC_LOSS + L2)', running_val_loss, epoch)

        if running_val_loss < best_loss - min_delta:
          best_loss = running_val_loss
          counter = 0 
        else:
          counter += 1

        if counter >= patience:
           print('Early stopping! Training stopped')
           break
          
        # if the epoch is a multiple of 10, save the weights of the model
        if epoch % 10 == 0:
          if continue_training:
            torch.save(model.state_dict(), os.path.join(weight_folder, f"ckpt_{epoch+starting_epoch+1}_epoch_{args.batch_size}_batchsize.pt"))
          else:
            torch.save(model.state_dict(), os.path.join(weight_folder, f"ckpt_{epoch}_epoch_{args.batch_size}_batchsize.pt"))
          for weight in os.listdir(weight_folder):
            name_list = weight.split('_')
            if (int(name_list[1])<epoch-patience) & (int(name_list[1])<epoch+starting_epoch+1-patience):
              os.remove(os.path.join(weight_folder, weight))
            # if (weight != f"ckpt_{epoch+starting_epoch}_epoch_{args.batch_size}_batchsize.pt") & (weight != f"ckpt_{epoch}_epoch_{args.batch_size}_batchsize.pt"):
            #   os.remove(os.path.join(weight_folder, weight))
    
    
    train_writer.close()
    val_writer.close()

    if len(os.listdir(weight_folder))>1:
      min_epoch=10000
      for weight in os.listdir(weight_folder):
        name_list = weight.split('_')
        if int(name_list[1])<min_epoch:
          min_epoch = int(name_list[1])

      for weight in os.listdir(weight_folder):
        name_list = weight.split('_')
        if int(name_list[1])!=min_epoch:
          os.remove(os.path.join(weight_folder, weight))

def launch():
    import argparse
    parser = argparse.ArgumentParser()
    args, unknown = parser.parse_known_args()
    args.epochs = 501
    args.batch_size = 16
    args.TOY_EXAMPLE_folder = os.getcwd()+os.path.sep+'TOY_EXAMPLE'
    args.root_dir = f'{args.TOY_EXAMPLE_folder}/DATA_FOR_FILTER'
    args.device = 'cuda'
    args.weight_folder = f'{os.getcwd()}/weights'
    args.continue_training = False
    args.starting_epoch = 160
    args.weight_path = f'{args.weight_folder}/ckpt_{args.starting_epoch}_epoch_{args.batch_size}_batchsize.pt'
    args.lr = 3e-4

    train(args)

if __name__ == '__main__':
    launch()
