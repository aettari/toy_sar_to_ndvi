import torch
import matplotlib.pyplot as plt
from PIL import Image, ImageFilter
import torchvision.transforms as transforms
import os
import random
import torchvision.transforms as T
from scripts.utils_toy import alpha_compositing



def unsharp_masker(array:torch.FloatTensor, radius=3, percent=80, max_unsharps=10, device='cuda') -> torch.FloatTensor:
    '''
    This function takes a torch tensor of images and applies a sharpening filter to them. The number of times the filter is applied
    is randomly chosen between 0 and max_unsharps. The radius and the percent of the filter are determined by radius and percent

    Input:
        -array: a torch tensor of images
        -radius: the radius of the filter
        -percent: the percent of the filter
        -max_unsharps: the maximum number of times the filter is applied
        -device: the device you want to use (default is cuda)
    
    Output:
        -a torch tensor of images shaped the same way of the input array
    '''
    unsharped_tensor = []
    for i in range(array.shape[0]):
        num_unsharp = random.choices(range(max_unsharps),k=1)[0]
        if num_unsharp == 0:
            unsharped_img = T.ToPILImage()(array[i])
        else:
            for j in range(num_unsharp):
                if j==0:
                    unsharped_img = T.ToPILImage()(array[i]).filter(ImageFilter.UnsharpMask(radius=radius, percent=percent))
                else:
                    unsharped_img = unsharped_img.filter(ImageFilter.UnsharpMask(radius=radius, percent=percent))

        unsharped_tensor.append(T.ToTensor()(unsharped_img))
    return torch.stack(unsharped_tensor, dim=0).to(device)

root_path = '/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/DATA_GausBlur/train/target_imgs'
target_paths = os.listdir(root_path)
transform = T.ToPILImage()
for i,idx in enumerate(random.choices(range(len(target_paths)), k=4)):
    unsharped_img = unsharp_masker(torch.load(root_path+'/'+target_paths[idx]), device='cpu')[0]

    imgs_cat_1 = torch.cat((torch.load(root_path+'/'+target_paths[idx]).to('cuda'), torch.load(root_path.replace('target_imgs','input_imgs')+'/'+target_paths[idx]).to('cuda')), dim=0)
    imgs_cat_2 = torch.cat((unsharped_img.to('cuda'), torch.load(root_path.replace('target_imgs','input_imgs')+'/'+target_paths[idx]).to('cuda')), dim=0)
    fig, axs = plt.subplots(1,5,figsize=(15,15))
    axs = axs.flatten()

    axs[0].imshow(transform(torch.load(root_path+'/'+target_paths[idx])), cmap='gray')
    axs[0].set_title('Original')
    axs[0].axis('off')
    axs[0].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
    
    axs[1].imshow(torch.load(root_path.replace('target_imgs','input_imgs')+'/'+target_paths[idx])[0].numpy(), cmap='gray')
    axs[1].set_title('Blurred')
    axs[1].axis('off')
    axs[1].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
    
    axs[2].imshow(unsharped_img[0].numpy(), cmap='gray')
    axs[2].set_title(f'Unsharped')
    axs[2].axis('off')
    axs[2].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
    
    axs[3].imshow(alpha_compositing(imgs_cat_1.detach().cpu(), [0.05,0.95], device='cpu'), cmap='gray')
    axs[3].set_title('Blurred + Original \n alpha blending before')
    axs[3].axis('off')
    axs[3].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
    
    axs[4].imshow(alpha_compositing(imgs_cat_2.detach().cpu(), [0.05,0.95], device='cpu'), cmap='gray')
    axs[4].set_title('Blurred + Unsharped \n alpha blending now')
    axs[4].axis('off')
    axs[4].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
    plt.show()
    
    


if __name__ == '__main__':
    pass