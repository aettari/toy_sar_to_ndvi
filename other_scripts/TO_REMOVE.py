import torch
import matplotlib.pyplot as plt
from PIL import Image, ImageFilter
import torchvision.transforms as transforms
import skimage
import numpy as np

def axs_plotter(axs,idx, img, title, cmap='gray', axis='off'):
    axs[idx].imshow(img, cmap=cmap)
    axs[idx].set_title(title)
    axs[idx].axis(axis)
    axs[idx].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
    
# blurred = torch.load('/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/DATA_GausBlur/test/input_imgs/12_Hugh_Jackman_21.pt')
real = torch.load('/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/DATA_GausBlur/test/target_imgs/12_Hugh_Jackman_21.pt')


transform = transforms.ToPILImage()
real = transform(real)
blurred = real.filter(ImageFilter.GaussianBlur(radius=3))
unsharped_skimage = transforms.ToTensor()(real.filter(ImageFilter.UnsharpMask(radius=3, percent=200)))
real = transforms.ToTensor()(real)
blurred = transforms.ToTensor()(blurred)
unsharped_adry = (real-blurred)*2. + real

fig, axs = plt.subplots(1,4,figsize=(15,15))
axs = axs.flatten()

axs_plotter(axs,0, blurred[0].numpy(), 'Blurred')
axs_plotter(axs,1, real[0].numpy(), 'Real')
axs_plotter(axs,2, unsharped_skimage[0].numpy(), 'Unsharped Skimage')
axs_plotter(axs,3, unsharped_adry[0].numpy(), 'Unsharped Adry')
plt.show()




# num_sharp = 10

# fig, axs = plt.subplots(1,num_sharp,figsize=(15,15))
# axs = axs.flatten()

# import torchvision.transforms as T
# transform = T.ToPILImage()
# for i in range(num_sharp):
#     if i == 0:
#         unsharped = (real - blurred)*0.8 + real
#         unsharped = (unsharped-unsharped.min())/(unsharped.max()-unsharped.min())

#     else:
#         blurred_image = transform(unsharped).filter(ImageFilter.GaussianBlur(radius=3))
#         blurred = transforms.ToTensor()(blurred_image)
#         unsharped = (unsharped - blurred)*0.8 + unsharped
#         unsharped = (unsharped-unsharped.min())/(unsharped.max()-unsharped.min())
#     axs[i].imshow(unsharped[0].numpy(), cmap='gray')
#     axs[i].set_title('Iteration: {}'.format(i+1))
#     axs[i].axis('off')
#     axs[i].tick_params(axis='both', which='both', bottom=False, left=False,
#                                 labelbottom=False, labelleft=False)

# plt.show()

# PIL
# import torchvision.transforms as T
# transform = T.ToPILImage()

# fig, axs = plt.subplots(1,10,figsize=(15,15))
# axs = axs.flatten()

# for i in range(num_sharp):
#     if i==0:
#         unsharped_img = transform(real).filter(ImageFilter.UnsharpMask(radius=3, percent=180))
#     else:
#         unsharped_img = unsharped_img.filter(ImageFilter.UnsharpMask(radius=3, percent=180))
#     axs[i].imshow(unsharped_img, cmap='gray')
#     axs[i].set_title('Iteration: {}'.format(i+1))
#     axs[i].axis('off')
#     axs[i].tick_params(axis='both', which='both', bottom=False, left=False,
#                                 labelbottom=False, labelleft=False)

# values, counts = np.unique(unsharped_img, return_counts=True)
# plt.show()

# # SKIMAGE
# from skimage import io
# from skimage.filters import unsharp_mask

# real = real[0].numpy()

# fig, axs = plt.subplots(1,10,figsize=(15,15))
# axs = axs.flatten()

# for i in range(10):
#     unsharped_img = unsharp_mask(real, radius=3, amount=0.8)
#     real = unsharped_img
#     axs[i].imshow(unsharped_img, cmap='gray')
#     axs[i].set_title('Iteration: {}'.format(i+1))
#     axs[i].axis('off')
#     axs[i].tick_params(axis='both', which='both', bottom=False, left=False,
#                                 labelbottom=False, labelleft=False)

# plt.show()

