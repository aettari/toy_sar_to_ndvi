import torch
import matplotlib.pyplot as plt
import os


fig, axs = plt.subplots(1,3, figsize=(15,5))
axs.flatten()

datasets = ['DATA_GausBlur10', 'DATA_GausBlur14', 'DATA']
imgs = os.listdir(f'/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/{datasets[0]}/train/target_imgs')
random_number = torch.randint(0, len(imgs), (1,)).item()
for i in range(3):
    file_path = f'/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/{datasets[i]}/train/target_imgs/{imgs[random_number]}'
    axs[i].imshow(torch.load(file_path)[0], cmap='gray')
    axs[i].axis('off')
    axs[i].set_title(f'{datasets[i]} \n {torch.load(file_path)[0].shape}')

plt.show()