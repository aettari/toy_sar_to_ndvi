# THIS SCRIPT IS JUST TO UNDERSTAND WHAT HAPPENS IN THE GAUSSIAN BLURRING (WE WILL TRUST THE PIL.ImageFilter FUNCTION)
import numpy as np
from scipy.ndimage import convolve
from PIL import Image

def gaussian_kernel(size, sigma):
    kernel = np.fromfunction(
        lambda x, y: (1 / (2 * np.pi * sigma**2)) * np.exp(-((x - size//2)**2 + (y - size//2)**2) / (2 * sigma**2)),
        (size, size)
    )
    return kernel / np.sum(kernel)

def gaussian_blur(image, size, sigma):
    kernel = gaussian_kernel(size, sigma)
    blurred_image = np.zeros_like(image, dtype=float)
    for channel in range(image.shape[-1]):
        blurred_image[..., channel] = convolve(image[..., channel], kernel, mode='constant')
    return np.uint8(blurred_image)



img_path = '/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/Celebrity_Faces_Dataset/Brad_Pitt/001_c04300ef.jpg'
img = np.array(Image.open(img_path))



# import matplotlib.pyplot as plt
# fig, axs = plt.subplots(1, 5, figsize=(15, 15))
# axs = axs.flatten()
# # sizes = [5,10,15,20,30]
# sigmas = [1.0,2.0,3.0,4.0,5.0]
# for i,sigma in enumerate(sigmas):
#     axs[i].imshow(gaussian_blur(img, size=40, sigma=sigma))
#     axs[i].axis('off')
#     axs[i].tick_params(axis='both', which='both', bottom=False, left=False,
#                                 labelbottom=False, labelleft=False)         
#     axs[i].set_title('Sigma: {}'.format(sigma))
# plt.show()


# import matplotlib.pyplot as plt
# plt.imshow(gaussian_kernel(10,5.0))
# plt.show()

