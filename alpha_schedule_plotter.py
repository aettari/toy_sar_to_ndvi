import torch
import numpy as np
import matplotlib.pyplot as plt

device ='cuda'
t_steps = 1000
type = 'sigmoid'

if type=='linear_1':
            all_alpha_input = torch.linspace(0,1,t_steps)
            all_alpha_target = torch.linspace(1,0,t_steps)
elif type=='linear_2':
    # FOR THIS YOU NEED TO SET A HIGH NUMBER OF TIME STEPS OTHERWISE all_alpha_input WON'T TERMINATE AT 0 BUT AT A LOT HIGHER VALUE
    all_alpha_input = torch.linspace(0.02,1e-4,t_steps)
    all_alpha_target= torch.cumprod(1-all_alpha_input, dim=0)
    all_alpha_input = 1-all_alpha_target
    
elif type=='cosine':
    def cosine_schedule(t, start=0, end=1, tau=1, clip_min=1e-9):
        # A gamma function based on cosine function.
        v_start = np.cos(start * np.pi / 2) ** (2 * tau)
        v_end = np.cos(end * np.pi / 2) ** (2 * tau)
        output = np.cos((t * (end - start) + start) * np.pi / 2) ** (2 * tau)
        output = (v_end - output) / (v_end - v_start)
        return np.clip(output, clip_min, 1.)
    all_alpha_input = torch.tensor(cosine_schedule(np.linspace(1,0,t_steps)), dtype=torch.float32)
    all_alpha_target = 1-all_alpha_input

elif type=='sigmoid':
    # from https://arxiv.org/pdf/2301.10972.pdf
    sigmoid = lambda x: 1/(1 + np.exp(-x))
    def sigmoid_schedule(t, start=0, end=1.5, tau=0.18, clip_min=1e-9):
        # tau controls the height of the elbow (the lower the steeper and viceversa)
        # start and end control where we cut the sigmoid function (you're better changing just the end and not the start)
        # A gamma function based on sigmoid function.
        v_start = sigmoid(start / tau)
        v_end = sigmoid(end / tau)
        output = sigmoid((t * (end - start) + start) / tau)
        output = (v_end - output) / (v_end - v_start)
        return np.clip(output, clip_min, 1.)
    all_alpha_target = torch.tensor(sigmoid_schedule(np.linspace(0,1,t_steps)), dtype=torch.float32)
    all_alpha_input = 1-all_alpha_target

        
else:
    raise ValueError('The type of alpha blending you specified is not valid. Please choose between linear_1, linear_2 and cosine')
    

all_alpha_input, all_alpha_target = all_alpha_input.to(device), all_alpha_target.to(device)

fig, axs = plt.subplots(1,2, figsize=(15, 5))
axs = axs.flatten()

axs[0].plot(all_alpha_input.cpu().numpy())
axs[1].plot(all_alpha_target.cpu().numpy())
plt.show()