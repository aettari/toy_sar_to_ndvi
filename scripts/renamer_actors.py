import os
import shutil

glob_path = os.path.join(os.dirname(os.getcwd()), 'Celebrity_Faces_Dataset')

actors = os.listdir(glob_path)

for actor in actors:
    actor_path = os.path.join(glob_path, actor)
    
    for i,img in enumerate(os.listdir(actor_path)):
        new_img = f'{actor}_{i}.jpg'
        os.rename(os.path.join(actor_path, img), os.path.join(actor_path, new_img))
        shutil.move(os.path.join(actor_path, new_img), os.path.join(glob_path, new_img))

    shutil.rmtree(actor_path)
    
