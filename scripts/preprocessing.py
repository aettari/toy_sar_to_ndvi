from PIL import Image, ImageFilter
import torch
import numpy as np
import os
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
from torchvision.transforms import InterpolationMode


# https://mmlab.ie.cuhk.edu.hk/projects/CelebA.html

def dataset_builder(root_path, starting_folder_name, dataset_name, resize_type, data_augmentation=False, list_gaus_blur_radius=None):
    '''
    INPUT:
        * root_path: path to the folder where the DATA folder will be created
        * starting_folder_name: name of the folder containing the images to be resized
        * dataset_name: name of the dataset to be created
        * resize_type: type of resizing to be applied to the images. It can be gaussian_blur and bilinear 
        * data_augmentation: boolean variable to decide whether to apply data augmentation or not (the data augmentation will be horizontal and vertical flip)
        * list_gaus_blur_radius: list of gaussian blur radius to be applied to the images. It is used only if resize_type is gaussian_blur

    OUTPUT:
        * None
    '''
    os.makedirs(root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs', exist_ok=True)
    os.makedirs(root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs', exist_ok=True)

    imgs_path = os.path.join(root_path, starting_folder_name)

    def min_max_scale(image):
        min_val = image.min()
        max_val = image.max()
        scaled_image = (image - min_val) / (max_val - min_val)
        return scaled_image
    
    transform_target = transforms.Compose([
        transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
        transforms.ToTensor(),
        transforms.Lambda(lambda x: x[0].unsqueeze(0)),
        transforms.Lambda(min_max_scale)
        ])

    if resize_type == 'gaussian_blur':

        if data_augmentation==False:
            for gaus_blur_radius in list_gaus_blur_radius:

                def gaussian_blurrer(image, radius=gaus_blur_radius):
                    return image.filter(ImageFilter.GaussianBlur(radius=radius))
                
                transform_input = transforms.Compose([
                    transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
                    transforms.Lambda(gaussian_blurrer),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                    ])
                
                for i,img_path in enumerate(os.listdir(imgs_path)):
                    img = Image.open(os.path.join(imgs_path, img_path))
                    input_img = transform_input(img)
                    target_img = transform_target(img)
                    torch.save(input_img, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'{gaus_blur_radius}_{img_path}'.replace('.jpg', '.pt'))
                    torch.save(target_img, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'{gaus_blur_radius}_{img_path}'.replace('.jpg', '.pt'))
                    # if i == 50000:#########################################################
                    #     break

        elif data_augmentation==True:
            for gaus_blur_radius in list_gaus_blur_radius:

                def gaussian_blurrer(image, radius=gaus_blur_radius):
                    return image.filter(ImageFilter.GaussianBlur(radius=radius))   
                
                transform_input = transforms.Compose([
                    transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
                    transforms.Lambda(gaussian_blurrer),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

                transform_input_horizontal_flip = transforms.Compose([
                    transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
                    transforms.Lambda(gaussian_blurrer),
                    transforms.RandomHorizontalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

                transform_input_vertical_flip = transforms.Compose([
                    transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
                    transforms.Lambda(gaussian_blurrer),
                    transforms.RandomVerticalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

                transform_target_horizontal_flip = transforms.Compose([
                    transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
                    transforms.RandomHorizontalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

                transform_target_vertical_flip = transforms.Compose([
                    transforms.Resize((224, 224), interpolation=InterpolationMode.BILINEAR),
                    transforms.RandomVerticalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

                for i,img_path in enumerate(os.listdir(imgs_path)):
                    img = Image.open(os.path.join(imgs_path, img_path))
                    input_img = transform_input(img)
                    input_img_horizontal_flip = transform_input_horizontal_flip(img)
                    input_img_vertical_flip = transform_input_vertical_flip(img)
                    target_img = transform_target(img)
                    target_img_horizontal_flip = transform_target_horizontal_flip(img)
                    target_img_vertical_flip = transform_target_vertical_flip(img)
                    torch.save(input_img, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'{gaus_blur_radius}_{img_path}'.replace('.jpg', '.pt'))
                    torch.save(target_img, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'{gaus_blur_radius}_{img_path}'.replace('.jpg', '.pt'))
                    torch.save(input_img_horizontal_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'{gaus_blur_radius}_H_{img_path}'.replace('.jpg', '.pt'))
                    torch.save(target_img_horizontal_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'{gaus_blur_radius}_H_{img_path}'.replace('.jpg', '.pt'))
                    torch.save(input_img_vertical_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'{gaus_blur_radius}_V_{img_path}'.replace('.jpg', '.pt'))
                    torch.save(target_img_vertical_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'{gaus_blur_radius}_V_{img_path}'.replace('.jpg', '.pt'))
                    # if i == 50000:#########################################################
                    #     break

        else:
            raise ValueError('data_augmentation must be either True or False')
                             
        
    elif resize_type == 'bilinear':
        if data_augmentation==True:
            transform_input = transforms.Compose([
                transforms.Resize(25, interpolation=InterpolationMode.BILINEAR),
                transforms.Resize((224, 224),interpolation=InterpolationMode.BILINEAR),
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                transforms.Lambda(min_max_scale)
            ])

            transform_input_horizontal_flip = transforms.Compose([
                    transforms.Resize(25, interpolation=InterpolationMode.BILINEAR),
                    transforms.Resize((224, 224),interpolation=InterpolationMode.BILINEAR),
                    transforms.RandomHorizontalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

            transform_input_vertical_flip = transforms.Compose([
                    transforms.Resize(25, interpolation=InterpolationMode.BILINEAR),
                    transforms.Resize((224, 224),interpolation=InterpolationMode.BILINEAR),
                    transforms.RandomVerticalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

            transform_target_horizontal_flip = transforms.Compose([
                    transforms.Resize((224, 224),interpolation=InterpolationMode.BILINEAR),
                    transforms.RandomHorizontalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])
            
            transform_target_vertical_flip = transforms.Compose([
                    transforms.Resize((224,224), interpolation=InterpolationMode.BILINEAR),
                    transforms.RandomVerticalFlip(p=1.0),
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                    transforms.Lambda(min_max_scale)
                ])

            for i,img_path in enumerate(os.listdir(imgs_path)):
                img = Image.open(os.path.join(imgs_path, img_path))
                input_img = transform_input(img)
                input_img_horizontal_flip = transform_input_horizontal_flip(img)
                input_img_vertical_flip = transform_input_vertical_flip(img)
                target_img = transform_target(img)
                target_img_horizontal_flip = transform_target_horizontal_flip(img)
                target_img_vertical_flip = transform_target_vertical_flip(img)
                torch.save(input_img, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'{img_path}'.replace('.jpg', '.pt'))
                torch.save(target_img, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'{img_path}'.replace('.jpg', '.pt'))
                torch.save(input_img_horizontal_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'H_{img_path}'.replace('.jpg', '.pt'))
                torch.save(target_img_horizontal_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'H_{img_path}'.replace('.jpg', '.pt'))
                torch.save(input_img_vertical_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'V_{img_path}'.replace('.jpg', '.pt'))
                torch.save(target_img_vertical_flip, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'V_{img_path}'.replace('.jpg', '.pt'))
                # if i == 50000:#########################################################
                #     break
        elif data_augmentation==False:
            transform_input = transforms.Compose([
                transforms.Resize(25, interpolation=InterpolationMode.BILINEAR),
                transforms.Resize((224, 224),interpolation=InterpolationMode.BILINEAR),
                transforms.Lambda(gaussian_blurrer),
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x[0].unsqueeze(0)),
                transforms.Lambda(min_max_scale)
            ])

            for i,img_path in enumerate(os.listdir(imgs_path)):
                img = Image.open(os.path.join(imgs_path, img_path))
                input_img = transform_input(img)
                target_img = transform_target(img)
                torch.save(input_img, root_path + os.path.sep + dataset_name + os.path.sep + 'input_imgs' + os.path.sep + f'{img_path}'.replace('.jpg', '.pt'))
                torch.save(target_img, root_path + os.path.sep + dataset_name + os.path.sep + 'target_imgs' + os.path.sep + f'{img_path}'.replace('.jpg', '.pt'))
                # if i == 50000:#########################################################
                #     break
            

## TRAIN TEST SPLIT
def train_val_test_splitter(datafolder, input_path, groundtruth_path, train_size=0.7, val_size = 0.2):
    '''
    This function creates the folder of the dataset and splits the dataset into train and test sets.
    The train and test sets are created by randomly selecting the images from the input_path and 
    groundtruth_path folders. Eventually, this function removes the input_path and the groundtruth_path folders
    
    INPUT:
        * datafolder: path to the folder where the train and test folders will be created
        * input_path: path to the folder containing the input images
        * groundtruth_path: path to the folder containing the groundtruth images
        * train_size: percentage of the dataset to be used for training
        * val_size: percentage of the dataset to be used for validation
    
    OUTPUT:
        * None
    '''
    import random
    import shutil
    os.makedirs(os.path.join(datafolder, 'train', 'target_imgs'), exist_ok=True)
    os.makedirs(os.path.join(datafolder, 'train', 'input_imgs'), exist_ok=True)
    os.makedirs(os.path.join(datafolder, 'val', 'target_imgs'), exist_ok=True)
    os.makedirs(os.path.join(datafolder, 'val', 'input_imgs'), exist_ok=True)
    os.makedirs(os.path.join(datafolder, 'test', 'target_imgs'), exist_ok=True)
    os.makedirs(os.path.join(datafolder, 'test', 'input_imgs'), exist_ok=True)

    shuffled_idxs = list(range(len(os.listdir(input_path))))
    random.shuffle(shuffled_idxs)
    train_idxs = shuffled_idxs[:int(len(shuffled_idxs)*train_size)]
    val_idxs = shuffled_idxs[int(len(shuffled_idxs)*train_size):int(len(shuffled_idxs)*(train_size+val_size))]
    test_idxs = shuffled_idxs[int(len(shuffled_idxs)*(train_size+val_size)):]
    
    file_names = os.listdir(input_path)
    
    for idx in train_idxs:
        shutil.copy(os.path.join(input_path, file_names[idx]), os.path.join(datafolder, 'train', 'input_imgs', file_names[idx]))
        shutil.copy(os.path.join(groundtruth_path, file_names[idx]), os.path.join(datafolder, 'train', 'target_imgs', file_names[idx]))
    for idx in val_idxs:
        shutil.copy(os.path.join(input_path, file_names[idx]), os.path.join(datafolder, 'val', 'input_imgs', file_names[idx]))
        shutil.copy(os.path.join(groundtruth_path, file_names[idx]), os.path.join(datafolder, 'val', 'target_imgs', file_names[idx]))
    for idx in test_idxs:
        shutil.copy(os.path.join(input_path, file_names[idx]), os.path.join(datafolder, 'test', 'input_imgs', file_names[idx]))
        shutil.copy(os.path.join(groundtruth_path, file_names[idx]), os.path.join(datafolder, 'test', 'target_imgs', file_names[idx]))

    shutil.rmtree(input_path)
    shutil.rmtree(groundtruth_path)


cwd = os.getcwd()
root_path = os.path.dirname(os.getcwd())
starting_dataset_name = 'Celebrity_Faces_Dataset'
dataset_name = 'DATA'
dataset_builder(root_path, starting_dataset_name, dataset_name, resize_type='gaussian_blur',data_augmentation=False, list_gaus_blur_radius=[15])
# dataset_builder(root_path, starting_dataset_name, dataset_name, resize_type='bilinear',data_augmentation=True)
dataset_path = root_path + os.path.sep + dataset_name
input_path = dataset_path + os.path.sep + 'input_imgs'
groundtruth_path = dataset_path + os.path.sep + 'target_imgs'
train_val_test_splitter(dataset_path, input_path, groundtruth_path, train_size=0.8, val_size = 0.15)