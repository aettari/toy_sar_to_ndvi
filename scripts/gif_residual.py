import imageio
import torch
from TOY_EXAMPLE.models.TOY_model_30.simple_UNet_model_12_toy import SimpleUnet
import numpy as np
import matplotlib.pyplot as plt
from TOY_EXAMPLE.utils_toy import alpha_schedule
import os

def gif_residual(args):
    '''

    '''
    t_steps = args.t_steps
    input_img_path = args.input_img_path
    target_img_path = args.target_img_path
    device = args.device
    weight_path = args.weight_path
    save_path = args.save_path
    type = args.type
    
    ckpt = torch.load(weight_path, map_location=torch.device(device))
    model = SimpleUnet().to(device)
    model.load_state_dict(ckpt)

    all_alpha_input, all_alpha_target = alpha_schedule(t_steps=t_steps, type=type)

    def min_max_scale(image):
        scaled_images = []
        for channel in range(image.shape[0]):
            channel_image = image[channel]
            min_val = channel_image.min()
            max_val = channel_image.max()
            scaled_channel_image = (channel_image - min_val) / (max_val - min_val)
            scaled_images.append(scaled_channel_image)

        scaled_tensor = torch.stack(scaled_images)
        return scaled_tensor
    # model.eval() ##############################################
    with torch.no_grad():
        input_img = torch.load(input_img_path).to(device)
        target_img = torch.load(target_img_path).to(device)

        frames = []
        for i in reversed(range(t_steps)):
            if i == t_steps - 1:
                alpha_blend_img = input_img * all_alpha_input[i] + target_img * all_alpha_target[i]
            else:
                alpha_blend_img = input_img * all_alpha_input[i] + predicted_target_img[0] * all_alpha_target[i]
            alpha_blend_img = alpha_blend_img.to(torch.float32).unsqueeze(0).to('cuda')
            # predicted_target_img = model(alpha_blend_img, torch.tensor([i], dtype=torch.float32).to('cuda'))
            predicted_difference_target_img = model(alpha_blend_img, torch.tensor([i], dtype=torch.float32).to('cuda'))
            predicted_target_img = input_img[0] + predicted_difference_target_img
            predicted_target_img = min_max_scale(predicted_target_img)
            target_minus_pred = target_img[0] - predicted_target_img[0][0]
            
            plt.hist(torch.flatten(target_minus_pred).detach().cpu().numpy(), bins=50, orientation='vertical')
            plt.title(f'hist(y-y^) t-step={i}', fontsize=30)
            plt.xlim(-1.25, 1.25)
            plt.ylim(0, 6500)
            filename = f'frame_{i}.png'
            plt.savefig(os.path.join(save_path,filename))
            
            # Clear the plot to prepare for the next frame
            plt.clf()
            # Append the image to the list of frames
            frames.append(imageio.imread(os.path.join(save_path,filename)))
            os.remove(os.path.join(save_path,filename))

        model.train()
        imageio.mimsave(os.path.join(save_path,'histogram_animation.gif'), frames, duration=0.25) 
    




def launch():
    import argparse
    import os
    parser = argparse.ArgumentParser()
    args, unknown = parser.parse_known_args()
    args.TOY_EXAMPLE_folder = os.getcwd()+"/TOY_EXAMPLE"
    args.input_img_path = os.path.join(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/',os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/')[0])
    args.device = 'cuda'
    args.model_name = 'TOY_model_30'
    args.weight_path = f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights/'+os.listdir(f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights/')[0]
    args.t_steps = 300
    args.target_img_path = os.path.join(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/target_imgs/',os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/target_imgs/')[0])
    args.save_path = f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}'
    args.type = 'sigmoid'
    gif_residual(args)

if __name__ == '__main__':
    launch()