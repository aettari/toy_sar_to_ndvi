import torch
from show_torch_image import show_torch_image
from models.simple_UNet_model_12 import SimpleUnet
from scripts.utils_toy import get_data
import torch.nn as nn
import math
import torch.nn.functional as F
# a = '/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/DATA/train/input_imgs/Angelina_Jolie_2.pt'
# show_torch_image(torch.load(a))
# a = '/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/DATA/train/target_imgs/Angelina_Jolie_2.pt'
# show_torch_image(torch.load(a))

class Block(nn.Module):
    def __init__(self, in_ch, out_ch, time_emb_dim,device='cuda', up=False):
        super().__init__()
        self.time_mlp =  nn.Linear(time_emb_dim, out_ch,device=device)
        if up:
            self.conv1 = nn.Conv2d(2*in_ch, out_ch, 3, padding=1, device=device)
            self.transform = nn.ConvTranspose2d(out_ch, out_ch, 4, 2, 1, device=device)
        else:
            self.conv1 = nn.Conv2d(in_ch, out_ch, 3, padding=1, device=device)
            self.transform = nn.Conv2d(out_ch, out_ch, 4, 2, 1, device=device)
        self.conv2 = nn.Conv2d(out_ch, out_ch, 3, padding=1, device=device)
        self.relu = nn.ReLU()
        self.batch_norm1 = nn.BatchNorm2d(out_ch, device=device)
        self.batch_norm2 = nn.BatchNorm2d(out_ch, device=device)
        
    def forward(self, x, t, ):
        # First Conv
        h = self.relu(self.batch_norm1(self.conv1(x)))
        # Time embedding
        time_emb = self.relu(self.time_mlp(t))
        # Extend last 2 dimensions
        time_emb = time_emb[(..., ) + (None, ) * 2]
        # Add time channel
        h = h + time_emb
        # Second Conv
        h = self.relu(self.batch_norm2(self.conv2(h)))
        # Down or Upsample
        return self.transform(h)

class SinusoidalPositionEmbeddings(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.dim = dim

    def forward(self, time):
        device = 'cuda'
        half_dim = self.dim // 2
        embeddings = math.log(10000) / (half_dim - 1)
        embeddings = torch.exp(torch.arange(half_dim, device=device) * -embeddings)
        embeddings = time[:, None] * embeddings[None, :]
        embeddings = torch.cat((embeddings.sin(), embeddings.cos()), dim=-1)
        # TODO: Double check the ordering here
        return embeddings

image_channels = 1 # it takes just one channel as input because we don't pass the stacked SAR, but the alpha_blending image generated from it
down_channels = (64, 128, 256, 512)
up_channels = (512, 256, 128, 64)
out_dim = 1 # it takes just one channel as output because the ndvi has just one channel
time_emb_dim =  64 # Refers to the number of dimensions or features used to represent time.
time_mlp = nn.Sequential(
                SinusoidalPositionEmbeddings(time_emb_dim),
                nn.Linear(time_emb_dim, time_emb_dim, device='cuda'),
                nn.ReLU(),
            )

conv0 = nn.Conv2d(image_channels, down_channels[0], 3, padding=1, device='cuda')

# Downsample
downs = nn.ModuleList([Block(down_channels[i], down_channels[i+1], \
                            time_emb_dim) \
            for i in range(len(down_channels)-1)])
# Upsample
ups = nn.ModuleList([Block(up_channels[i], up_channels[i+1], \
                                time_emb_dim, up=True) \
            for i in range(len(up_channels)-1)])
# Output
output = nn.Conv2d(up_channels[-1], out_dim, 1)




def forward(x, timestep):
    t = time_mlp(timestep)
    x = conv0(x)
    # Unet
    residual_inputs = []
    for down in downs:
        x = down(x, t)
        residual_inputs.append(x)
    
    for up in ups:
            residual_x = residual_inputs.pop() # I need to start from the last one and moving to the first one; that's why we use .pop()
            # Add residual x as additional channels
            if residual_x.shape != x.shape:
                residual_x = F.interpolate(residual_x, size=x.shape[-2:], mode='bilinear', align_corners=False)
            # print(x.shape)  
            # print(residual_x.shape)
            x = torch.cat((x, residual_x), dim=1) # residual connection between layers (1024 with 1024; 512 with 512 etc.)        
            x = up(x, t)
    return x




batch_size = 1
root_dir = '/home/VICOMTECH/aettari/Desktop/toy_sar_to_ndvi/DATA'

# model = SimpleUnet().to('cuda')

train_dataset = get_data(root_dir, 'train')
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
t = torch.randint(low=1, high=5, size=(batch_size,)).to('cuda')

for batch_idx, (data, target) in enumerate(train_loader):
    data = data.to('cuda')
    target = target.to('cuda')

    x = forward(data,t)

    break
print(x.shape)
