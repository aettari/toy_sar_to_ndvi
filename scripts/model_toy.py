import torch
import torch.nn as nn
import torch.nn.functional as F

class SelfAttention(nn.Module):
    def __init__(self, channels, size, device='cuda'):
        super(SelfAttention, self).__init__()
        self.device = device
        self.channels = channels       # store the number of channels
        self.size = size               # store the size of input image
        self.mha = nn.MultiheadAttention(channels, 4, batch_first=True, device=self.device)   # Define a multi-head attention layer with 4 heads and set batch_first to True (which indicates that the first dimension is the batch dimension)
        self.ln = nn.LayerNorm([channels], device=self.device) # Remember that LayerNorm normalizes each object independently through the object features (channels)
            
        # Define a feedforward neural network with two linear layers and a GELU activation function    
        self.ff_self = nn.Sequential(
            nn.LayerNorm([channels], device=self.device),
            nn.Linear(channels, channels, device=self.device),
            nn.GELU(),
            nn.Linear(channels, channels, device=self.device),
        )

    def forward(self, x):
        # Reshape the input tensor to have dimensions (-1, channels, size * size) and swap the 1st and 2nd dimensions
        x = x.view(-1, self.channels, self.size * self.size).swapaxes(1, 2) # In computer vision tasks, it's common to flatten the spatial dimensions of an image before feeding it into certain layers like self-attention.This allows the model to treat different spatial positions as separate instances, making it easier to apply sequence-based operations.
        x_ln = self.ln(x) # layernormalization can help improve the overall training stability
        attention_value, _ = self.mha(x_ln, x_ln, x_ln) # Putting x_ln as the query, key and value of the multi-head attention layer, we get the self attention
        attention_value = attention_value + x
        attention_value = self.ff_self(attention_value) + attention_value
        return attention_value.swapaxes(2, 1).view(-1, self.channels, self.size, self.size)
    
class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None, residual=False, device='cuda'):
        '''
        We have just two convolutional layers, therefore we just need in_channels, mid_channels (what 
        the first convolution must return) and out_channels (what the second convolution must return).
        You can also not use the mid_channels and then the first convolution will return out_channels.

        There's also a little bit of ResNet in this class (in case residual=True)
        '''
        super().__init__()
        self.device = device
        self.residual = residual # If True, the output of the double convolution will be the sum of the input and the output of the double convolution.
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False, device=self.device),
            # GroupNorm() performs the same formula of BatchNorm() but on a smaller group of channels (not on the whole batch).
            # This can help to reduce the memory and computational requirements of the normalization layer. So, it takes as input
            # the number of groups (images) to separate the channels into (remember that the array of an image batch is (batch_size, 
            # channels, height, width)) and the number of channels to normalize and it returns as output the normalized array of the
            # same shape. If num_groups = 1, then it is equivalent to having a batch size of batch_size=1 in nn.BatchNorm2d,
            # where each image is normalized independently.
            nn.GroupNorm(1, mid_channels, device = self.device),
            # GELU() (Gaussian Error Linear Unit) is a variant of the ReLU activation function. 
            # It is a smooth approximation of the ReLU function.
            # It is defined as: GELU(x) = 0.5 * x * (1 + tanh(sqrt(2 / pi) * (x + 0.044715 * x^3))) but actually 
            # the formula that you must remember is               GELU(x) = x * P(X <= x) 
            # where P(X <= x) is the CDF of the Gaussian (usually a standard one, but one could also set mu and sigma as learnable
            # parameters).
            # That's why usually if comes after a BatchNorm layer or a GroupNorm layer.
            # GELU is kind of a mix between ReLU and a Dropout layer because
            # for values of x particularly smaller than 0 (e.g. -2. Remember that in a standard Gaussian
            # the pdf for x<-2 is basically 0), GELU outputs 0 and so turn off the neuron. We can say that on the
            # extremes acts like a Relu (because if x>1 it approximate to x (because P(X<1)=1 and therefore
            # GELU(x>1)=x)). It solves the Dying ReLU problem and performs better than ReLU (indeed OpenAI used it in their GPT-3 model).
            nn.GELU(),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False, device=self.device),
            nn.GroupNorm(1, out_channels, device=self.device),
        )

    def forward(self, x):
        '''
        Performs a forward pass through the DoubleConv network with input x and returns
        the output of the network.
        '''
        if self.residual:
            #if residual=True, apply a residual connection between
            return F.gelu(x + self.double_conv(x)) # residual connection
        else:
            return self.double_conv(x)

class Down(nn.Module):
    '''
    The main part consists in reducing the size of the image by a factor of 2 (with a MaxPool2d layer) and then
    applying two Double convolutional layers. 

    The second part is the embedding layer. Since most blocks differ in terms of the hidden dimensions from the time 
    step embedding we'll make use of a linear projection to lead the time embedding in the proper dimension. 
    This consists of a SiLU activation and then a Linear projection which moves the time embedding from the emb_dim dimension
    to hidden dimensions.

    In the forward pass we first feed images to the convolutional block and project the time embedding accordingly. 
    Then we add both together and return.

    NOTICE THAT emb_dim MUST BE THE DIMENSION OF t 
    '''
    def __init__(self, in_channels, out_channels, emb_dim=64, device = 'cuda'):
        super().__init__()
        self.device = device
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2), # downsample the image by a factor of 2
            # apply two double convolutional layers to the downsampled image
            DoubleConv(in_channels, in_channels, residual=True), # you can apply the residual connection because x and double_conv(x) have the same shape
            DoubleConv(in_channels, out_channels),
        )

        self.emb_layer = nn.Sequential(
            nn.SiLU(),   # apply a SiLU activation function to the time embedding
            nn.Linear(emb_dim, out_channels, device = self.device),   # apply a linear projection to the time embedding to move it from emb_dim dimension to hidden dimensions

        )

    def forward(self, x, t):
        x = self.maxpool_conv(x)   #feed the images to the convolutional block
        emb = self.emb_layer(t)[:, :, None, None].repeat(1, 1, x.shape[-2], x.shape[-1])   #project the time embedding
        return x + emb   #add the images and the projected time embedding and return the result

def pos_encoding(t, channels):
        inv_freq = 1.0 / (
            10000
            ** (torch.arange(0, channels, 2, device='cuda').float() / channels)
        )
        pos_enc_a = torch.sin(t.repeat(1, channels // 2) * inv_freq)
        pos_enc_b = torch.cos(t.repeat(1, channels // 2) * inv_freq)
        pos_enc = torch.cat([pos_enc_a, pos_enc_b], dim=-1)
        return pos_enc

class Up(nn.Module):
    '''
    It's almost the same of the Down class but instead of having a MaxPool2d layer we have an Upsample layer.
    '''
    def __init__(self, in_channels, out_channels, emb_dim=64,device='cuda'):
        super().__init__()
        self.device = device
        self.up = nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True)
        self.conv = nn.Sequential(
            DoubleConv(in_channels, in_channels, residual=True),
            DoubleConv(in_channels, out_channels, in_channels // 2),
        )

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(emb_dim, out_channels,device = self.device),
        )

    def forward(self, x, skip_x, t):
        x = self.up(x)
        x = torch.cat([skip_x, x], dim=1) # concaatenate the skip connection with the upsampled image
        x = self.conv(x)
        emb = self.emb_layer(t)[:, :, None, None].repeat(1, 1, x.shape[-2], x.shape[-1]).to(self.device)
        return x + emb
    

class UNet(nn.Module):
    def __init__(self, c_in=1, c_out=1, time_dim=64, device="cuda"):

        super().__init__()
        self.device = device
        self.time_dim = time_dim
        self.inc = DoubleConv(c_in, 4)
        self.down1 = Down(4, 8)
        self.sa1 = SelfAttention(8, 64)
        self.down2 = Down(8, 16)
        self.sa2 = SelfAttention(16, 32)
        self.down3 = Down(16, 16)
        self.sa3 = SelfAttention(16, 16)

        self.bot1 = DoubleConv(16, 32) 
        self.bot2 = DoubleConv(32, 32)
        self.bot3 = DoubleConv(32, 16)

        self.up1 = Up(32, 16)
        self.sa4 = SelfAttention(16, 32)
        self.up2 = Up(24, 8)
        self.sa5 = SelfAttention(8, 64) 
        self.up3 = Up(12, 4)
        # self.sa6 = SelfAttention(4, 128) # LET'S TRY REMOVING THIS FOR CUDA OUT OF MEMORY PROBLEMS   
        self.outc = nn.Conv2d(4, c_out, kernel_size=1)

    def pos_encoding(self, t, channels):
        inv_freq = 1.0 / (
            10000
            ** (torch.arange(0, channels, 2, device=self.device).float() / channels)
        )
        pos_enc_a = torch.sin(t.repeat(1, channels // 2) * inv_freq)
        pos_enc_b = torch.cos(t.repeat(1, channels // 2) * inv_freq)
        pos_enc = torch.cat([pos_enc_a, pos_enc_b], dim=-1)
        return pos_enc

    def forward(self, x, t):
        t = t.unsqueeze(-1).type(torch.float)
        t = self.pos_encoding(t, self.time_dim)

        x1 = self.inc(x)
        x2 = self.down1(x1, t)
        x2 = self.sa1(x2)
        x3 = self.down2(x2, t)
        x3 = self.sa2(x3)
        x4 = self.down3(x3, t)
        x4 = self.sa3(x4)

        x4 = self.bot1(x4)
        x4 = self.bot2(x4)
        x4 = self.bot3(x4)

        x = self.up1(x4, x3, t)
        x = self.sa4(x)
        x = self.up2(x, x2, t)
        x = self.sa5(x)
        x = self.up3(x, x1, t)
        # x = self.sa6(x) # LET'S TRY REMOVING THIS FOR CUDA OUT OF MEMORY PROBLEMS   
        output = self.outc(x)
        return output
    
model = UNet()
print("Num params: ", sum(p.numel() for p in model.parameters()))
model