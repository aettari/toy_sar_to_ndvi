import torch
from TOY_EXAMPLE.models.TOY_model_28.simple_UNet_model_12_toy import SimpleUnet
from TOY_EXAMPLE.utils_toy import plot_target_predicted, alpha_schedule
import numpy as np
import matplotlib.pyplot as plt
from pytorch_msssim import ssim, ms_ssim
import os

def sample(args):
        '''
        As the name suggests this function is used for sampling. Therefore, we want to 
        loop backward (moreover, notice that in the sample we want to perform EVERY STEP CONTIGUOUSLY) because 
        in diffusion models the inference consists in moving from the input distribution to the target distribution. 

        Input:
            -time_steps: the number of time steps we have to perform
            -input_img_path: the input image we want to convert into target image (in the TOY example we want to convert the blurred image into the original image)
            -device: cpu or cuda
            -weight_path: the weight path of the trained model
            -type: the type of alpha schedule we want to use (linear_1, linear_2, cosine, sigmoid)
        
        Output:
            x: a tensor shaped as the input target representing the prediction of the target
        '''

        time_steps = args.time_steps
        input_img_path = args.input_img_path
        device = args.device
        weight_path = args.weight_path
        type = args.type

        ckpt = torch.load(weight_path, map_location=torch.device(device))
        model = SimpleUnet().to(device)
        # model = UNet().to(device)
        model.load_state_dict(ckpt)

        all_alpha_input, all_alpha_target = alpha_schedule(t_steps=time_steps, type=type)

        # model.eval() ##############################################
        with torch.no_grad(): # disables gradient calculation
            input_img = torch.load(input_img_path).to(device)
            predicted_target_img = torch.zeros(input_img.shape[1], input_img.shape[2]).to(device)
            # predicted_difference_target_img = torch.zeros(input_img.shape[1], input_img.shape[2]).to(device)
            # predicted_target_img = input_img[0]+predicted_difference_target_img
            for t in reversed(range(time_steps)):
                alpha_blend_img = input_img[0]*all_alpha_input[t]  + predicted_target_img*all_alpha_target[t]
                alpha_blend_img = alpha_blend_img.to(torch.float32).unsqueeze(0).unsqueeze(0).to(device)
                predicted_target_img = model(alpha_blend_img, torch.tensor([t], dtype=torch.float32).to(device))[0][0]
        model.train()

        return predicted_target_img

def sample_grid(args):
    '''
    This function plots a grid of the backward steps. Each image in the grid is a concatenation of the alpha blending image that
    we pass as input to the network and the predicted image from the previous step.
    Moreover, it plots as title of each image the MAE and the SSIM between the alpha blending image and the predicted
    image for each backward step.
    '''
    input_img_path = args.input_img_path
    target_img_path = args.target_img_path
    device = args.device
    weight_path = args.weight_path
    save_path = args.grid_save_path
    type = args.type
    time_steps_grid = args.time_steps_grid
    
    ckpt = torch.load(weight_path, map_location=torch.device(device))
    model = SimpleUnet().to(device)
    # model = UNet().to(device)
    model.load_state_dict(ckpt)

    all_alpha_input, all_alpha_target = alpha_schedule(t_steps=time_steps_grid, type=type)

    # IF YOU DON'T ASSIGN A NUMBER TO time_steps_grid WHOSE SQUARE ROOT IS AN INTEGER THEN THE CODE WILL CONVERT IT TO THE NEAREST
    # INTEGER WHOSE SQUARE ROOT IS AN INTEGER
    time_steps_grid = int(np.sqrt(time_steps_grid))**2

    fig, axs = plt.subplots(nrows=int(np.sqrt(time_steps_grid)), ncols=int(np.sqrt(time_steps_grid)), figsize=(25,25))
    axs = axs.flatten()
    # model.eval() #############################
    # Grid plot
    def min_max_scale(image):
        scaled_images = []
        for channel in range(image.shape[0]):
            channel_image = image[channel]
            min_val = channel_image.min()
            max_val = channel_image.max()
            scaled_channel_image = (channel_image - min_val) / (max_val - min_val)
            scaled_images.append(scaled_channel_image)

        scaled_tensor = torch.stack(scaled_images)
        return scaled_tensor


    with torch.no_grad():
        input_img = torch.load(input_img_path).to(device)
        target_img = torch.load(target_img_path).to(device)              
        for i in reversed(range(time_steps_grid)):
            if i == time_steps_grid-1:
                alpha_blend_img = input_img*all_alpha_input[i] + target_img*all_alpha_target[i] # At the first step we don't have the prediction (the alpha blending is just composed by the input img)
            else:
                alpha_blend_img = input_img*all_alpha_input[i]+ predicted_target_img[0]*all_alpha_target[i]
            alpha_blend_img = alpha_blend_img.to(torch.float32).unsqueeze(0).to(device)
            predicted_target_img = model(alpha_blend_img, torch.tensor([i], dtype=torch.float32).to(device))
            # predicted_difference_target_img = model(alpha_blend_img, torch.tensor([i], dtype=torch.float32).to(device))
            # predicted_target_img = input_img+predicted_difference_target_img
            predicted_target_img = min_max_scale(predicted_target_img)
            axs[i].imshow(torch.cat([alpha_blend_img[0][0],predicted_target_img[0][0]],dim=1).detach().cpu().numpy(), cmap='gray')
            axs[i].tick_params(axis='both', which='both', bottom=False, left=False,
                                labelbottom=False, labelleft=False)
            mae_real_pred = torch.mean(torch.abs(target_img-predicted_target_img[0][0]))
            ssim_pred = ssim(target_img.unsqueeze(0),predicted_target_img,data_range=1, size_average=True)
            axs[i].set_title(f'Inp-Pred-{i} \n MAE: {mae_real_pred.item():.4f} \n SSIM: {ssim_pred.item():.4f}',fontsize=10)
            # ms_ssim_pred = ms_ssim(target_img.unsqueeze(0),predicted_target_img,data_range=1, size_average=True)
            # axs[i].set_title(f'Inp-Pred-{i} \n MAE: {mae_real_pred.item():.4f} \n SSIM: {ssim_pred.item():.4f} \n MS-SSIM: {ms_ssim_pred.item():.4f}',fontsize=10)

    model.train()
    fig.savefig(save_path)

def launch():
    import argparse
    import os
    parser = argparse.ArgumentParser()
    args, unknown = parser.parse_known_args()
    args.TOY_EXAMPLE_folder = os.getcwd()+"/TOY_EXAMPLE"
    args.input_img_path = os.path.join(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/',os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/input_imgs/')[0])
    args.device = 'cuda'
    args.model_name = 'TOY_model_28'
    args.weight_path = f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights/'+os.listdir(f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/weights/')[0]
    args.time_steps_grid = 50
    args.target_img_path = os.path.join(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/target_imgs/',os.listdir(f'{args.TOY_EXAMPLE_folder}/DATA_bilinear/test/target_imgs/')[0])
    args.grid_save_path = f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/results/alphablend_prediction_GRID.png'
    args.type = 'sigmoid'

    # for time_step in [3,20,50,100,200]:
    #     args.time_steps = time_step
    #     predicted_target = sample(args).detach().cpu() # You don't need to write .numpy() because it is done in the plot_target_predicted() function
    #     plot_target_predicted(torch.load(args.input_img_path),torch.load(args.target_img_path), predicted_target,title=f'{args.time_steps} TIME STEPS TARGET PREDICITON',
    #                           save_path=f'{args.TOY_EXAMPLE_folder}/models/{args.model_name}/results/prediction_{args.time_steps}steps.png')


    sample_grid(args)


if __name__ == '__main__':
    launch()